// console.log("Hello World!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printWelcomeMessage(){
		let userFullName = prompt("Enter your Full Name: ");
		let userAge = prompt("Enter your Age: ");
		let userLocation = prompt("Enter your Location: ");

		console.log("Hello, " + userFullName + "!");
		console.log("You are " + userAge + " years old.");
		console.log("You live in " + userLocation + ".");
	}
	printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function displayFavBands(){
		console.log("1. My Chemical Romance");
		console.log("2. All American Rejects");
		console.log("3. Linkin Park");
		console.log("4. Taking Back Sunday");
		console.log("5. Coldplay");
	}
	displayFavBands();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function displayFavMovies(){
		console.log("1. Eternal Sunshine of a Spotless Mind");
		console.log("Rotten Tomatoes Rating: 92%");
		console.log("2. 500 Days of Summer");
		console.log("Rotten Tomatoes Rating: 85%");
		console.log("3. Parasite");
		console.log("Rotten Tomatoes Rating: 99%");
		console.log("4. Spirited Away");
		console.log("Rotten Tomatoes Rating: 97%");
		console.log("5. Your Name");
		console.log("Rotten Tomatoes Rating: 98%");
	}
	displayFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers(); -> cannot call function without declaring first
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");	 
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

/*console.log(friend1);
console.log(friend2); ->cannot access function scoped variable inside a function
*/
